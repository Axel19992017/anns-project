/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Data;

import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;

import pojo.Empleado;

/**
 *
 * @author TSIS-08
 */
public class EmpleadoData {

    private Empleado[] empleados;

    public EmpleadoData() {
        try {
            populateEmpleados();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EmpleadoData.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static class EmpleadoSingleton {

        public static final EmpleadoData EDATA = new EmpleadoData();
    }

    private void populateEmpleados() throws FileNotFoundException {
        Gson gson = new Gson();
        empleados = gson.fromJson(new FileReader("empleado_data.json"), Empleado[].class);
    }

    public static EmpleadoData getInstance() {
        return EmpleadoSingleton.EDATA;
    }

    public static void main(String[] args) {
        EmpleadoData edata = EmpleadoData.getInstance();
        for (Empleado e : edata.empleados) {
            System.out.println(e.toString());
        }
    }
}
