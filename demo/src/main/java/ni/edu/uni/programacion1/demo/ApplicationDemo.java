/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ni.edu.uni.programacion1.demo;

import ni.edu.uni.programacion1.pojo.Empleado;

/**
 *
 * @author Sistemas36
 */
public class ApplicationDemo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Empleado empleados[] = {
            new Empleado();
            new Empleado(11, "00-020287-0000U", "Ana", "Petronila", "Lopez",
                    "Lopez", "km 5", "-", "88756714")
            new Empleado(12, "0001-020695-0001U", "Armando", "Jose",
                    "Guerra", "Paz", "km 5", "-", "8811156")
        };
        /*Empleado ePepito = new Empleado();
        Empleado eAna = new Empleado(11, "00-020287-0000U", "Ana", "Petronila", "Lopez", "Lopez", "km 5", "-", "88756714");*/
        
        printEmpleado(eAna);
        System.out.println("---------------------");
        printEmpleado(ePepito);
        System.out.println(ePepito.toString());
        System.out.println("================");
        System.out.println(eAna.toString());
        
        
        
    }
    
    public static void printEmpleado(Empleado e){
        System.out.println("Codigo: " + e.getCodigo());
        System.out.println("Nombre: " + e.getPrimerNombre());
        System.out.println("Apellido: " + e.getPrimerApellido());
        System.out.println("Direccion: " + e.getDireccion());
        System.out.println("Telefono: " + e.getTelefono());
        System.out.println("Celular: " + e.getCelular());
    }
    
}
