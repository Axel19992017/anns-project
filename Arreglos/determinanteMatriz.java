import java.util.Scanner;
import java.util.Arrays;
public class determinanteMatriz{
    public static void main(String[] args) {
        Scanner y = new Scanner(System.in);
        int b;
        int matriz[][];
        System.out.print("Cantidad de filas y columnas: ");
        b = y.nextInt();
        matriz = new int[b][b];
        System.out.println("Digite los elementos de la matriz: ");
        for(int i=0;i<matriz.length;i++){
            for(int j=0;j<matriz[i].length;j++){
                System.out.print("["+i+"]"+"["+j+"]:");
                matriz[i][j]= y.nextInt();
            }
        }
        System.out.println("***********************************");
        mostrarMatriz(matriz);
        System.out.println("***********************************");
        determinanteM(matriz, b);
        System.out.println("***********************************");
        adjuntaMatriz(matriz, b);
        System.out.println("***********************************");
    
        
    }
    public static void mostrarMatriz(int A[][]){
        for(int i = 0; i < A.length; i++)
        {
            System.out.println(Arrays.toString(A[i]));
        }
    }
    public static void determinanteM(int x[][], int b){
        int a, y, z, i, j, k, det = 0;
        switch (b) {
            case 2:
                a = (x[0][0]*(x[1][1]));
                y = (x[1][0]*(x[0][1]));
                det = (a-y);
                System.out.println("El determinante es: " + det);
                break;
            case 3:
                a = (x[0][0])*(x[1][1])*(x[2][2]);
                y = (x[1][0])*(x[2][1])*(x[0][2]);
                z = (x[2][0])*(x[0][1])*(x[1][2]);
                i = (x[2][0])*(x[1][1])*(x[0][2]);
                j = (x[0][0])*(x[2][1])*(x[1][2]);
                k = (x[1][0])*(x[0][1])*(x[2][2]);
                det = ((a+y+z)-(i+j+k));
                System.out.println("El determinante es: " + det);
                break;
            default:
                System.out.println("No se encontro determinante. ");
                break;
        }
    }
    public static void adjuntaMatriz(int x[][], int b){
        int c, d, e, f, g, h, i, j,k;
        int [][]transpuesta = new int[b][b];
        float inversa[][];
        switch (b) {
            case 2:
                System.out.println("No hay");
                break;
            case 3:
                c = ((x[1][1])*(x[2][2]))-((x[2][1])*(x[1][2]));
                d = -(((x[1][0])*(x[2][2]))-((x[2][0])*(x[1][2])));
                e = ((x[1][0])*(x[2][1]))-((x[2][0])*(x[1][1]));
                f = -(((x[0][1])*(x[2][2]))-((x[2][1])*(x[0][2])));
                g = ((x[0][0])*(x[2][2]))-((x[2][0])*(x[0][2]));
                h = -(((x[0][0])*(x[2][1]))-((x[2][0])*(x[0][1])));
                i = ((x[0][1])*(x[1][2]))-((x[1][1])*(x[0][2]));
                j = -(((x[0][0])*(x[1][2]))-((x[1][0])*(x[0][2])));
                k = ((x[0][0])*(x[1][1]))-((x[1][0])*(x[0][1]));
                int nueva[][] = {
                    {c,d,e},
                    {f,g,h},
                    {i,j,k}
                };
                System.out.println("Matriz adjunta: ");
                
                for(int a=0;a<nueva.length;a++){
                    System.out.println(Arrays.toString(nueva[a]));
                }
                System.out.println("Matriz transpuesta: ");
                for(int a=0;a<3;a++){
                    for (int q=0;q<3;q++){
                       transpuesta[q][a] = nueva[a][q];
                       
                    }
                }
                System.out.print("******************************\n");
                
                for(int a=0;a<3;a++){
                    System.out.println(Arrays.toString(transpuesta[a]));
                    
                }

                break;
            default:
                System.out.println("Opcion invalida.");
                break;
        }
    }
    /*public static void transpuestaMatriz(int x[][], int b){
        int [][]transpuesta = new int[b][b];
        adjuntaMatriz(x, b);
        for(int i=0;i<3;i++){
            for (int j=0;j<3;j++){
               transpuesta[j][i] = x[i][j];
            }
        }
        System.out.print("******************************\n");
        
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(transpuesta[i][j] + "\t");
            }
            System.out.println();
        }
    }*/

}