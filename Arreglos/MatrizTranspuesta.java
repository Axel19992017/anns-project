import java.util.Arrays;
public class MatrizTranspuesta{
    public static void main(String[] args){
        int A[][] = {
            {2,1,-3,5},
            {1,-4,7,6}
        };
        int transpuesta[][] = new int[4][2];
        printByRow(A);
        
        for(int i = 0; i < 2; i++){
            for (int j = 0; j < 4; j++){
               transpuesta[j][i] = A[i][j];
            }
        }
        System.out.print("******************************\n");
        
        for(int i = 0; i < 4; i++){
            for(int j = 0; j < 2; j++){
                System.out.print(transpuesta[i][j] + "\t");
            }
            System.out.println();
        }
        
    
    }
    public static void printByRow(int A[][]){
        for(int i = 0; i < A.length; i++)
        {
            System.out.println(Arrays.toString(A[i]));
        }
    }
   
    
}