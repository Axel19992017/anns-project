import java.util.Scanner;
public class SumaDiagonal{
    public static void main(String[] args){
        Scanner y = new Scanner(System.in);
        int a[][];
        int suma = 0, fila, col;
        do{
            System.out.println("Cantidad de filas: ");
            fila = y.nextInt();
            System.out.println("Cantidad de columnas: ");
            col = y.nextInt();

        }while( fila<=0 && col<=0);
        a = new int[fila][col];
        if(fila != col){
            System.out.println("La matriz debe ser una matriz cuadrada.");
        }
        else{
            System.out.println("Digite los elementos de la matriz: ");
            for(int i=0;i<a.length;i++){
                for(int j=0;j<a[i].length;j++){
                    System.out.print("["+i+"]"+"["+j+"]:");
                    a[i][j]= y.nextInt();
                }
            }
            for(int i=0;i<a.length;i++){
                for(int j=0;j<a[i].length;j++){
                    if(i!=j){
                        suma += a[i][j];
                    }
                }
            }
            System.out.println("La suma es: " + suma);
        }

    }
}