import java.util.Scanner;
public class SumaMatriz{
    public static void main(String[] args){
        Scanner y = new Scanner(System.in);
        int suma = 0, fila, col;
        int M[][];
        do{
            System.out.println("Cantidad de filas: ");
            fila = y.nextInt();
            System.out.println("Cantidad de columnas: ");
            col = y.nextInt();

        }while( fila<=0 && col<=0);
        M = new int[fila][col];
        if(fila != col){
            System.out.println("La matriz debe ser una matriz cuadrada.");
        }
        else{
            System.out.println("Digite los elementos de la matriz: ");
            for(int i=0;i<M.length;i++){
                for(int j=0;j<M[i].length;j++){
                    System.out.print("["+i+"]"+"["+j+"]:");
                    M[i][j]= y.nextInt();
                    suma += M[i][j];
                }
            }
            System.out.println("La suma de todos sus elementos es: " + suma);
        }
        // Hacer la suma de cada fila y de cada columna

    }
}