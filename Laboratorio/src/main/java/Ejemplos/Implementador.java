/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplos;

/**
 *
 * @author Sistemas36
 */
import Ejemplos.pojo.Camioneta;
import java.util.Scanner;

class Implementador {
 public static void main(String args[]) {
    // Creamos un Objeto de Tipo Caminioneta e ingresaremos sus datos.
    Scanner scan = new Scanner(System.in);
    Camioneta c = new Camioneta();
    // Obtención de Datos a través de los métodos heredados de la clase Vehículo
    System.out.print("Ingrese la Marca:");
    c.SetMarca(scan.next());
    System.out.print("Ingrese el Modelo:");
    c.SetModelo(scan.next());
    System.out.print("Ingrese la Capacidad de Pasajeros:");
    c.SetPasajeros(scan.nextInt());
    System.out.print("Ingrese el Color:");
    c.SetColor(scan.next());
    // Obtención de datos con los métodos de la clase hija Camioneta
    System.out.println("Ingrese el Tipo de Camioneta:");
    System.out.println("1. Doble Cabina");
    System.out.println("2. Cabina sencilla");
    System.out.println("3. SUV");
    int tipo = scan.nextInt();
    c.setTipo_camioneta(Camioneta.TIPO_CAMIONETA.values()[tipo-1]);
    System.out.println("Doble Traccion:");
    System.out.println("1. Si");
    System.out.println("2. No");
    int dobleT = scan.nextInt();
    boolean values[] = {true, false};
    c.setDoble_traccion(values[dobleT - 1]);
    // Impresión de los datos en consola
    System.out.println("Los datos ingresados fueron: ");
    System.out.println("Marca: " + c.GetMarca());
    System.out.println("Modelo: " + c.GetModelo());
    System.out.println("Cantidad de Pasajeros: " + c.GetPasajeros());
    System.out.println("Color: " + c.GetColor());
    System.out.println("Tipo de Camioneta: " + c.getTipo_camioneta());
    System.out.println("Doble Traccion: " + (c.getDoble_traccion()?"Si":"No"));
    }
 
}
