/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplos.pojo;

/**
 *
 * @author Sistemas36
 */
public class Camioneta extends Vehiculo {
    // heredando de la clase Vehiculo a travéz de la palabra reservada extends
    public enum TIPO_CAMIONETA {
        DOBLE_CABINA, CABINA_SENCILLA, SUV
    };
    private TIPO_CAMIONETA tipo_camioneta;
    private Boolean doble_traccion;
    public Camioneta() {
        super();
    }

    public Camioneta(String marca, String modelo, int pasajeros, String color) {
        super(marca, modelo, pasajeros, color);
    }
    public Camioneta(TIPO_CAMIONETA tipo_camioneta, Boolean doble_traccion, String marca,
        String modelo, int pasajeros, String color) {
        super(marca, modelo, pasajeros, color);
        this.tipo_camioneta = tipo_camioneta;
        this.doble_traccion = doble_traccion;
    }
    public Boolean getDoble_traccion() {
        return doble_traccion;
    }
    public void setDoble_traccion(Boolean doble_traccion) {
        this.doble_traccion = doble_traccion;
    }
    public TIPO_CAMIONETA getTipo_camioneta() {
        return tipo_camioneta;
    }
    public void setTipo_camioneta(TIPO_CAMIONETA tipo_camioneta) {
        this.tipo_camioneta = tipo_camioneta;
    }
    
}
