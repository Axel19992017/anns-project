/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ejemplos.pojo;

/**
 *
 * @author Sistemas36
 */
public class Vehiculo {
    private String marca;
    private String modelo;
    private int pasajeros;
    private String color;
    
    public Vehiculo() {
    }
    public Vehiculo(String marca, String modelo, int pasajeros, String color) {
    this.marca = marca;
    this.modelo = modelo;
    this.pasajeros = pasajeros;
    this.color = color;
 }
    public void SetMarca(String marca) {
       this.marca = marca;
    }
    public String GetMarca() {
       return this.marca;
    }

    public void SetModelo(String modelo) {
        this.modelo = modelo;
    }
    public String GetModelo() {
        return this.modelo;
    }

    public void SetPasajeros(int pasajeros) {
        this.pasajeros = pasajeros;
    }
    public int GetPasajeros() {
        return this.pasajeros;
    }

    public void SetColor(String color) {
        this.color = color;
    }
    public String GetColor() {
        return this.color;
    }
    
}
