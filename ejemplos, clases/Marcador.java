/*Objeto java simple
    Plain Old Java Object (POJO)
    JavaBeans
*/
public class Marcador{
    /*Atributos de la clase*/
    private String color;   // dado que es object su valor por defecto es null
    private int tamano; //0
    private String tipo; // dado que es object su valor por defecto es null

    /* Metodo constructor por defecto, permite inicializar
       los atributos de los objetos instaciados
    */

    public Marcador(){ // Comportamiento       
    }
    // Objeto java simple
    public Marcador(String color, int tamano, String tipo){
        this.color = color; //"this" es para diferenciar la variable de la clase y la del paràmetro
        this.tamano = tamano;
        this.tipo = tipo;
    }

    public void setColor(String color){
       this.color = color;
    }

    public String getColor(){
        return this.color;
    }

    public void setTamano(int tamano){
        this.tamano = tamano;
    }

    public int getTamano(){
        return this.tamano;
    }

    public void setTipo(String tipo){
        this.tipo = tipo;
    }

    public String getTipo(){
        return this.tipo;
    }

}