public class MarcadorDemo{
    public static void main(String[] args) {
        Marcador m = new Marcador();
        Marcador m1 = new Marcador("Negro", 10, "Permanente");


        System.out.println("Color:" + m.getColor());
        System.out.println("Tamaño: " + m.getTamano());
        System.out.println("Tipo: " + m.getTipo());

        m.setColor("Azul");
        m.setTamano(5);
        m.setTipo("Acrilico");

        System.out.println("<<==============================================>>");
        System.out.println("Color:" + m.getColor());
        System.out.println("Tamano: " + m.getTamano());
        System.out.println("Tipo: " + m.getTipo());

        System.out.println("<<==============================================>>");
        System.out.println("Color:" + m1.getColor());
        System.out.println("Tamano: " + m1.getTamano());
        System.out.println("Tipo: " + m1.getTipo());





    }
}