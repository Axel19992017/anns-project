/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package ni.edu.uni.programacion1.data;

import java.util.Arrays;
import static java.util.Collections.list;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import ni.edu.uni.programacion1.enums.Sexo;
import ni.edu.uni.programacion1.pojo.Empleado;

/**
*
* @author Sistemas40
*/
public class EmpleadoData {
    private Empleado[] empleados;
    private MunicipioData mData;

    public EmpleadoData(){
        mData = new MunicipioData();
    }

    public void add(Empleado e){
        empleados = addEmpleado(empleados,e);
    }
    
    public Empleado[] getEmpleados(){
        return this.empleados;
    }
    public Empleado getEmpleadoByCodigo(int codigo){
        if(codigo <= 0){
            return null;
        }
        Empleado e = new Empleado();
        e.setCodigo(codigo);
        Comparator<Empleado> c = new Comparator<Empleado>(){
            @Override
            public int compare(Empleado arg0, Empleado arg1){
                return (arg0.getCodigo() - arg1.getCodigo());
            }
        };
        Arrays.sort(empleados, c);
        
        int index = Arrays.binarySearch(empleados,e,c);
        return empleados[index];
        
    }
    public Empleado getEmpleadoByCedula(String cedula){
        if(cedula == null){
            return null;
        }
        if(cedula.isBlank() || cedula.isEmpty()){
            return null;
        }
        Empleado e = new Empleado();
        e.setCedula(cedula);
        Arrays.sort(empleados);
        int index = Arrays.binarySearch(empleados, e);
        if(index < 0){
            return null;
        }
        return empleados[index];
    }
    public Empleado[] getEmpleadosBySexo(Sexo sexo){
       List<Empleado> list = Arrays.asList(empleados).stream().filter(e -> e.getSexo() == sexo).collect(Collectors.toList());
       return (Empleado[])list.toArray();
    }
    
    

    private Empleado[] addEmpleado(Empleado[] eCopy, Empleado e){
        if(eCopy == null){
        eCopy = new Empleado[1];
        eCopy[eCopy.length -1] = e;
        return eCopy;
    }

    eCopy = Arrays.copyOf(eCopy, eCopy.length + 1);
    eCopy[eCopy.length -1] = e;

    return eCopy;
    }
    
}