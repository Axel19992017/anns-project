public class Ejercicio3{
    public static void main (String[] args){
        int grados = 30;
        double g, k;
        System.out.println("Grados Celsius: " + grados);
        System.out.println("***Transformacion***");
        g = (grados * 1.8) + 32;
        System.out.println("Fahrenheit: " + g);
        k = grados + 273.15;
        System.out.println("Grados Kelvin: " + k);
    }
}