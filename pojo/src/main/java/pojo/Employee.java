/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pojo;

/**
 *
 * @author Sistemas36
 */
public class Employee {
    private int id;
    private String names;
    private String lastnames;
    private String address;
    private String phone;
    private float salary;

    public Employee() {
    }

    public Employee(int id, String names, String lastnames, String address, String phone, float salary) {
        this.id = id;
        this.names = names;
        this.lastnames = lastnames;
        this.address = address;
        this.phone = phone;
        this.salary = salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getLastnames() {
        return lastnames;
    }

    public void setLastnames(String lastnames) {
        this.lastnames = lastnames;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", names=" + names + ", lastnames=" + lastnames + ", address=" + address + ", phone=" + phone + ", salary=" + salary + '}';
    }
    
    
    
}
