/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.data;
/* https://mockaroo.com/ genera datos fakes */
import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.io.FileReader;
import pojo.Employee;

/**
 *
 * @author Sistemas36
 */
public class EmployeeData {
    private Employee[] employees;

    public EmployeeData() throws FileNotFoundException {
        populateEmployees();
    }

    public Employee[] getEmployees() {
        return employees;
    }
    
    private void populateEmployees() throws FileNotFoundException { 
        // Metodo para agregar los empleados en el arreglo
        Gson gson = new Gson();
        // Convierte arreglos de tipo Json a uno de tipo Java
        employees = gson.fromJson(new FileReader("employees_data.json"), Employee[].class); 
    }
    public Employee[] getEmployeesBySalaryRange(float min, float max){
        // Crear un metodo que muestre los empleados con menor salario
        Employee[] employeesBySalary = null;
        for(Employee e : employees){
            if(e.getSalary() >= min && e.getSalary() <= max){
                employeesBySalary = addEmployee(employeesBySalary, e);
            }
        }
       return employeesBySalary;
    }
    private Employee[] addEmployee(Employee[] tmp, Employee e){
        if(tmp == null){
            tmp = new Employee[1];
            tmp[tmp.length - 1] = e;
            return tmp;
            
        }
        tmp = Arrays.CopyOf()
    }
}
