/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uni.pojo;

import java.io.FileNotFoundException;
import java.util.logging.Level;
import java.util.logging.Logger;
import pojo.Employee;
import uni.data.EmployeeData;

/**
 *
 * @author Sistemas36
 */
public class Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            EmployeeData eData = new EmployeeData();
            Employee[] employees = eData.getEmployeesBySalaryRange(6000, 10000);
            System.out.println("Total: " + employees.length);
            for(Employee e : employees){
                System.out.println(e.toString());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Application.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
